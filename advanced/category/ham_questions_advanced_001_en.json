[
  {
    "id": "A-001-001-001",
    "language": "en",
    "question": "What is the meaning of the term \"time constant\" in an RL circuit ?",
    "options": {
      "A": "The time required for the voltage in the circuit to build up to 36.8% of the maximum value",
      "B": "The time required for the current in the circuit to build up to 63.2% of the maximum value",
      "C": "The time required for the voltage in the circuit to build up to 63.2% of the maximum value",
      "D": "The time required for the current in the circuit to build up to 36.8% of the maximum value"
    },
    "answer": {
      "B": "The time required for the current in the circuit to build up to 63.2% of the maximum value"
    }
  },
  {
    "id": "A-001-001-002",
    "language": "en",
    "question": "What is the term for the time required for the capacitor in an RC circuit to be charged to 63.2% of the supply voltage?",
    "options": {
      "A": "One exponential period",
      "B": "An exponential rate of one",
      "C": "A time factor of one",
      "D": "One time constant"
    },
    "answer": {
      "D": "One time constant"
    }
  },
  {
    "id": "A-001-001-003",
    "language": "en",
    "question": "What is the term for the time required for the current in an RL circuit to build up to 63.2% of the maximum value?",
    "options": {
      "A": "A time factor of one",
      "B": "One exponential rate",
      "C": "One time constant",
      "D": "An exponential period of one"
    },
    "answer": {
      "C": "One time constant"
    }
  },
  {
    "id": "A-001-001-004",
    "language": "en",
    "question": "What is the term for the time it takes for a charged capacitor in an RC circuit to discharge to 36.8% of its initial value of stored charge?",
    "options": {
      "A": "One time constant",
      "B": "One discharge period",
      "C": "A discharge factor of one",
      "D": "An exponential discharge of one"
    },
    "answer": {
      "A": "One time constant"
    }
  },
  {
    "id": "A-001-001-005",
    "language": "en",
    "question": "What is meant by \"back EMF\"?",
    "options": {
      "A": "An opposing EMF equal to R times C percent of the applied EMF",
      "B": "A voltage that opposes the applied EMF",
      "C": "A current that opposes the applied EMF",
      "D": "A current equal to the applied EMF"
    },
    "answer": {
      "B": "A voltage that opposes the applied EMF"
    }
  },
  {
    "id": "A-001-001-006",
    "language": "en",
    "question": "After two time constants, the capacitor in an RC circuit is charged to what percentage of the supply voltage?",
    "options": {
      "A": "36.8%",
      "B": "95%",
      "C": "86.5%",
      "D": "63.2%"
    },
    "answer": {
      "C": "86.5%"
    }
  },
  {
    "id": "A-001-001-007",
    "language": "en",
    "question": "After two time constants, the capacitor in an RC circuit is discharged to what percentage of the starting voltage?",
    "options": {
      "A": "63.2%",
      "B": "13.5%",
      "C": "86.5%",
      "D": "36.8%"
    },
    "answer": {
      "B": "13.5%"
    }
  },
  {
    "id": "A-001-001-008",
    "language": "en",
    "question": "What is the time constant of a circuit having a 100 microfarad capacitor in series with a 470 kilohm resistor?",
    "options": {
      "A": "470 seconds",
      "B": "0.47 seconds",
      "C": "4700 seconds",
      "D": "47 seconds"
    },
    "answer": {
      "D": "47 seconds"
    }
  },
  {
    "id": "A-001-001-009",
    "language": "en",
    "question": "What is the time constant of a circuit having a 470 microfarad capacitor in series with a 470 kilohm resistor?",
    "options": {
      "A": "47 000 seconds",
      "B": "221 seconds",
      "C": "470 seconds",
      "D": "221 000 seconds"
    },
    "answer": {
      "B": "221 seconds"
    }
  },
  {
    "id": "A-001-001-010",
    "language": "en",
    "question": "What is the time constant of a circuit having a 220 microfarad capacitor in series with a 470 kilohm resistor?",
    "options": {
      "A": "470 000 seconds",
      "B": "470 seconds",
      "C": "220 seconds",
      "D": "103 seconds"
    },
    "answer": {
      "D": "103 seconds"
    }
  },
  {
    "id": "A-001-002-001",
    "language": "en",
    "question": "What is the result of skin effect?",
    "options": {
      "A": "Thermal effects on the surface of the conductor decrease impedance",
      "B": "As frequency decreases, RF current flows in a thinner layer of the conductor, closer to the surface",
      "C": "As frequency increases, RF current flows in a thinner layer of the conductor, closer to the surface",
      "D": "Thermal effects on the surface of the conductor increase impedance"
    },
    "answer": {
      "C": "As frequency increases, RF current flows in a thinner layer of the conductor, closer to the surface"
    }
  },
  {
    "id": "A-001-002-002",
    "language": "en",
    "question": "What effect causes most of an RF current to flow along the surface of a conductor?",
    "options": {
      "A": "Resonance effect",
      "B": "Layer effect",
      "C": "Skin effect",
      "D": "Piezoelectric effect"
    },
    "answer": {
      "C": "Skin effect"
    }
  },
  {
    "id": "A-001-002-003",
    "language": "en",
    "question": "Where does almost all RF current flow in a conductor?",
    "options": {
      "A": "In the centre of the conductor",
      "B": "Along the surface of the conductor",
      "C": "In a magnetic field in the centre of the conductor",
      "D": "In a magnetic field around the conductor"
    },
    "answer": {
      "B": "Along the surface of the conductor"
    }
  },
  {
    "id": "A-001-002-004",
    "language": "en",
    "question": "Why does most of an RF current flow within a very thin layer under the conductor's surface?",
    "options": {
      "A": "Because of heating of the conductor's interior",
      "B": "Because the RF resistance of a conductor is much less than the DC resistance",
      "C": "Because of skin effect",
      "D": "Because a conductor has AC resistance due to self-inductance"
    },
    "answer": {
      "C": "Because of skin effect"
    }
  },
  {
    "id": "A-001-002-005",
    "language": "en",
    "question": "Why is the resistance of a conductor different for RF currents than for direct currents?",
    "options": {
      "A": "Because conductors are non-linear devices",
      "B": "Because of skin effect",
      "C": "Because of the Hertzberg effect",
      "D": "Because the insulation conducts current at high frequencies"
    },
    "answer": {
      "B": "Because of skin effect"
    }
  },
  {
    "id": "A-001-002-006",
    "language": "en",
    "question": "What unit measures the ability of a capacitor to store electrical charge?",
    "options": {
      "A": "Watt",
      "B": "Farad",
      "C": "Coulomb",
      "D": "Volt"
    },
    "answer": {
      "B": "Farad"
    }
  },
  {
    "id": "A-001-002-007",
    "language": "en",
    "question": "A wire has a current passing through it. Surrounding this wire there is:",
    "options": {
      "A": "a skin effect that diminishes with distance",
      "B": "an electromagnetic field",
      "C": "a cloud of electrons",
      "D": "an electrostatic field"
    },
    "answer": {
      "B": "an electromagnetic field"
    }
  },
  {
    "id": "A-001-002-008",
    "language": "en",
    "question": "In what direction is the magnetic field oriented about a conductor in relation to the direction of electron flow?",
    "options": {
      "A": "In the same direction as the current",
      "B": "In the direct opposite to the current",
      "C": "In all directions",
      "D": "In the direction determined by the left-hand rule"
    },
    "answer": {
      "D": "In the direction determined by the left-hand rule"
    }
  },
  {
    "id": "A-001-002-009",
    "language": "en",
    "question": "What is the term for energy that is stored in an electromagnetic or electrostatic field?",
    "options": {
      "A": "Ampere-joules",
      "B": "Kinetic energy",
      "C": "Potential energy",
      "D": "Joule-coulombs"
    },
    "answer": {
      "C": "Potential energy"
    }
  },
  {
    "id": "A-001-002-010",
    "language": "en",
    "question": "Between the charged plates of a capacitor there is:",
    "options": {
      "A": "a magnetic field",
      "B": "an electric current",
      "C": "a cloud of electrons",
      "D": "an electrostatic field"
    },
    "answer": {
      "D": "an electrostatic field"
    }
  },
  {
    "id": "A-001-002-011",
    "language": "en",
    "question": "Energy is stored within an inductor that is carrying a current. The amount of energy depends on this current, but it also depends on a property of the inductor. This property has the following unit:",
    "options": {
      "A": "henry",
      "B": "farad",
      "C": "watt",
      "D": "coulomb"
    },
    "answer": {
      "A": "henry"
    }
  },
  {
    "id": "A-001-003-001",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit if R is 47 ohms, L is 50 microhenrys and C is 40 picofarads?",
    "options": {
      "A": "7.96 MHz",
      "B": "79.6 MHz",
      "C": "3.56 MHz",
      "D": "1.78 MHz"
    },
    "answer": {
      "C": "3.56 MHz"
    }
  },
  {
    "id": "A-001-003-002",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 40 microhenrys and C is 200 picofarads?",
    "options": {
      "A": "1.99 MHz",
      "B": "1.78 MHz",
      "C": "1.78 kHz",
      "D": "1.99 kHz"
    },
    "answer": {
      "B": "1.78 MHz"
    }
  },
  {
    "id": "A-001-003-003",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 50 microhenrys and C is 10 picofarads?",
    "options": {
      "A": "3.18 MHz",
      "B": "7.12 kHz",
      "C": "3.18 kHz",
      "D": "7.12 MHz"
    },
    "answer": {
      "D": "7.12 MHz"
    }
  },
  {
    "id": "A-001-003-004",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 25 microhenrys and C is 10 picofarads?",
    "options": {
      "A": "10.1 kHz",
      "B": "10.1 MHz",
      "C": "63.7 kHz",
      "D": "63.7 MHz"
    },
    "answer": {
      "B": "10.1 MHz"
    }
  },
  {
    "id": "A-001-003-005",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 3 microhenrys and C is 40 picofarads?",
    "options": {
      "A": "14.5 MHz",
      "B": "13.1 MHz",
      "C": "13.1 kHz",
      "D": "14.5 kHz"
    },
    "answer": {
      "A": "14.5 MHz"
    }
  },
  {
    "id": "A-001-003-006",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 4 microhenrys and C is 20 picofarads?",
    "options": {
      "A": "17.8 kHz",
      "B": "19.9 MHz",
      "C": "17.8 MHz",
      "D": "19.9 kHz"
    },
    "answer": {
      "C": "17.8 MHz"
    }
  },
  {
    "id": "A-001-003-007",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 8 microhenrys and C is 7 picofarads?",
    "options": {
      "A": "2.84 MHz",
      "B": "21.3 MHz",
      "C": "2.13 MHz",
      "D": "28.4 MHz"
    },
    "answer": {
      "B": "21.3 MHz"
    }
  },
  {
    "id": "A-001-003-008",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 3 microhenrys and C is 15 picofarads?",
    "options": {
      "A": "23.7 kHz",
      "B": "35.4 MHz",
      "C": "23.7 MHz",
      "D": "35.4 kHz"
    },
    "answer": {
      "C": "23.7 MHz"
    }
  },
  {
    "id": "A-001-003-009",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 4 microhenrys and C is 8 picofarads?",
    "options": {
      "A": "28.1 MHz",
      "B": "49.7 kHz",
      "C": "49.7 MHz",
      "D": "28.1 kHz"
    },
    "answer": {
      "A": "28.1 MHz"
    }
  },
  {
    "id": "A-001-003-010",
    "language": "en",
    "question": "What is the resonant frequency of a series RLC circuit, if R is 47 ohms, L is 1 microhenry and C is 9 picofarads?",
    "options": {
      "A": "17.7 MHz",
      "B": "53.1 MHz",
      "C": "5.31 MHz",
      "D": "1.77 MHz"
    },
    "answer": {
      "B": "53.1 MHz"
    }
  },
  {
    "id": "A-001-003-011",
    "language": "en",
    "question": "What is the value of capacitance (C) in a series R-L-C circuit, if the circuit resonant frequency is 14.25 MHz and L is 2.84 microhenrys?",
    "options": {
      "A": "2.2 picofarads",
      "B": "44 microfarads",
      "C": "44 picofarads",
      "D": "2.2 microfarads"
    },
    "answer": {
      "C": "44 picofarads"
    }
  },
  {
    "id": "A-001-004-001",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 1 microhenry and C is 10 picofarads?",
    "options": {
      "A": "50.3 MHz",
      "B": "15.9 kHz",
      "C": "50.3 kHz",
      "D": "15.9 MHz"
    },
    "answer": {
      "A": "50.3 MHz"
    }
  },
  {
    "id": "A-001-004-002",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 2 microhenrys and C is 15 picofarads?",
    "options": {
      "A": "29.1 kHz",
      "B": "5.31 MHz",
      "C": "29.1 MHz",
      "D": "5.31 kHz"
    },
    "answer": {
      "C": "29.1 MHz"
    }
  },
  {
    "id": "A-001-004-003",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 5 microhenrys and C is 9 picofarads?",
    "options": {
      "A": "3.54 MHz",
      "B": "23.7 kHz",
      "C": "3.54 kHz",
      "D": "23.7 MHz"
    },
    "answer": {
      "D": "23.7 MHz"
    }
  },
  {
    "id": "A-001-004-004",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 2 microhenrys and C is 30 picofarads?",
    "options": {
      "A": "20.5 MHz",
      "B": "2.65 MHz",
      "C": "20.5 kHz",
      "D": "2.65 kHz"
    },
    "answer": {
      "A": "20.5 MHz"
    }
  },
  {
    "id": "A-001-004-005",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 15 microhenrys and C is 5 picofarads?",
    "options": {
      "A": "2.12 MHz",
      "B": "2.12 kHz",
      "C": "18.4 MHz",
      "D": "18.4 kHz"
    },
    "answer": {
      "C": "18.4 MHz"
    }
  },
  {
    "id": "A-001-004-006",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 3 microhenrys and C is 40 picofarads?",
    "options": {
      "A": "1.33 MHz",
      "B": "1.33 kHz",
      "C": "14.5 kHz",
      "D": "14.5 MHz"
    },
    "answer": {
      "D": "14.5 MHz"
    }
  },
  {
    "id": "A-001-004-007",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 40 microhenrys and C is 6 picofarads?",
    "options": {
      "A": "10.3 kHz",
      "B": "10.3 MHz",
      "C": "6.63 MHz",
      "D": "6.63 kHz"
    },
    "answer": {
      "B": "10.3 MHz"
    }
  },
  {
    "id": "A-001-004-008",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 10 microhenrys and C is 50 picofarads?",
    "options": {
      "A": "7.12 kHz",
      "B": "7.12 MHz",
      "C": "3.18 MHz",
      "D": "3.18 kHz"
    },
    "answer": {
      "B": "7.12 MHz"
    }
  },
  {
    "id": "A-001-004-009",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 200 microhenrys and C is 10 picofarads?",
    "options": {
      "A": "3.56 kHz",
      "B": "7.96 kHz",
      "C": "7.96 MHz",
      "D": "3.56 MHz"
    },
    "answer": {
      "D": "3.56 MHz"
    }
  },
  {
    "id": "A-001-004-010",
    "language": "en",
    "question": "What is the resonant frequency of a parallel RLC circuit if R is 4.7 kilohms, L is 90 microhenrys and C is 100 picofarads?",
    "options": {
      "A": "1.68 MHz",
      "B": "1.77 MHz",
      "C": "1.77 kHz",
      "D": "1.68 kHz"
    },
    "answer": {
      "A": "1.68 MHz"
    }
  },
  {
    "id": "A-001-004-011",
    "language": "en",
    "question": "What is the value of inductance (L) in a parallel RLC circuit, if the resonant frequency is 14.25 MHz and C is 44 picofarads?",
    "options": {
      "A": "253.8 millihenrys",
      "B": "0.353 microhenry",
      "C": "3.9 millihenrys",
      "D": "2.8 microhenrys"
    },
    "answer": {
      "D": "2.8 microhenrys"
    }
  },
  {
    "id": "A-001-005-001",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 14.128 MHz, L is 2.7 microhenrys and R is 18 kilohms?",
    "options": {
      "A": "0.013",
      "B": "7.51",
      "C": "75.1",
      "D": "71.5"
    },
    "answer": {
      "C": "75.1"
    }
  },
  {
    "id": "A-001-005-002",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 14.128 MHz, L is 4.7 microhenrys and R is 18 kilohms?",
    "options": {
      "A": "4.31",
      "B": "13.3",
      "C": "0.023",
      "D": "43.1"
    },
    "answer": {
      "D": "43.1"
    }
  },
  {
    "id": "A-001-005-003",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 4.468 MHz, L is 47 microhenrys and R is 180 ohms?",
    "options": {
      "A": "13.3",
      "B": "0.00735",
      "C": "0.136",
      "D": "7.35"
    },
    "answer": {
      "C": "0.136"
    }
  },
  {
    "id": "A-001-005-004",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 14.225 MHz, L is 3.5 microhenrys and R is 10 kilohms?",
    "options": {
      "A": "71.5",
      "B": "0.0319",
      "C": "31.9",
      "D": "7.35"
    },
    "answer": {
      "C": "31.9"
    }
  },
  {
    "id": "A-001-005-005",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 7.125 MHz, L is 8.2 microhenrys and R is 1 kilohm?",
    "options": {
      "A": "36.8",
      "B": "0.368",
      "C": "0.273",
      "D": "2.73"
    },
    "answer": {
      "D": "2.73"
    }
  },
  {
    "id": "A-001-005-006",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 7.125 MHz, L is 10.1 microhenrys and R is 100 ohms?",
    "options": {
      "A": "22.1",
      "B": "0.221",
      "C": "0.00452",
      "D": "4.52"
    },
    "answer": {
      "B": "0.221"
    }
  },
  {
    "id": "A-001-005-007",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 7.125 MHz, L is 12.6 microhenrys and R is 22 kilohms?",
    "options": {
      "A": "25.6",
      "B": "22.1",
      "C": "39",
      "D": "0.0256"
    },
    "answer": {
      "C": "39"
    }
  },
  {
    "id": "A-001-005-008",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 3.625 MHz, L is 3 microhenrys and R is 2.2 kilohms?",
    "options": {
      "A": "25.6",
      "B": "0.031",
      "C": "32.2",
      "D": "31.1"
    },
    "answer": {
      "C": "32.2"
    }
  },
  {
    "id": "A-001-005-009",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 3.625 MHz, L is 42 microhenrys and R is 220 ohms?",
    "options": {
      "A": "2.3",
      "B": "0.00435",
      "C": "0.23",
      "D": "4.35"
    },
    "answer": {
      "C": "0.23"
    }
  },
  {
    "id": "A-001-005-010",
    "language": "en",
    "question": "What is the Q of a parallel RLC circuit, if it is resonant at 3.625 MHz, L is 43 microhenrys and R is 1.8 kilohms?",
    "options": {
      "A": "23",
      "B": "54.3",
      "C": "0.543",
      "D": "1.84"
    },
    "answer": {
      "D": "1.84"
    }
  },
  {
    "id": "A-001-005-011",
    "language": "en",
    "question": "Why is a resistor often included in a parallel resonant circuit ?",
    "options": {
      "A": "To decrease the Q and increase the resonant frequency",
      "B": "To increase the Q and decrease bandwidth",
      "C": "To decrease the Q and increase the bandwidth",
      "D": "To increase the Q and decrease the skin effect"
    },
    "answer": {
      "C": "To decrease the Q and increase the bandwidth"
    }
  }
]